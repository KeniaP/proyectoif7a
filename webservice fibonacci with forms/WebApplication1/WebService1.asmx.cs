﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication1
{
    /// <summary>
    /// Fibonacci Sequence generator via ASMX Web Service
    /// Tudor Laptes, 05/18/17
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        //[WebMethod]
        public WebService1()
        {
            //esta parte de aqui es la inicializacion de codigo como c# public stactic al incio de todo el codigo
        }
        [WebMethod(Description = "Show Message")]
        public string msgshow()
        {
            return "Hello, Web Service...";
        }
        [WebMethod(Description = "Addition Of Two Integers")]
        //public int add(int a, int b)
        public  long fibonacciRecursivo(long n)
        {
            if (n == 0) return 0;
            else if (n == 1) return 1;
            else return fibonacciRecursivo(n - 1) + fibonacciRecursivo(n - 2);//Llamada recursiva con el elemento n-1 + n-2 
        }
    }
}
