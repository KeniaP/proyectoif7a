﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Net.Http;
using WindowsFormsApplication13.localhost;
using System.Threading;

namespace WindowsFormsApplication13
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /**********************************************************************************/
        //se crearon 3 veces el procedimiento para evitar error en caso del que profesor no utilize los 3 textbox al hacerlo todo junto
        //iba marcar error por falta de datos en los texbox blanco, en cambio al separar el procedimiento dandole a cada textbox
        // su procedimiento si los demas tuvieran valor o no ya no seria problema
        /********************************************************************************************/

        //metodo de task para primer textbox
        private Task<string> LongRunningMethodAsync(string message)
        {
            // este es el metodo donde por medio del task mandamos a llamar el mensaje que queremos mostrar
            return Task.Run<string>(() => LongRunningMethod(message));
        }

        //metodo de task para segundo textbox
        private Task<string> LongRunningMethodAsync2(string message)
        {
            // este es el metodo donde por medio del task mandamos a llamar el mensaje que queremos mostrar
            return Task.Run<string>(() => LongRunningMethod2(message));
        }
        //metodo de task para tercer textbox
        private Task<string> LongRunningMethodAsync3(string message)
        {
            // este es el metodo donde por medio del task mandamos a llamar el mensaje que queremos mostrar
            return Task.Run<string>(() => LongRunningMethod3(message));
        }

        /**********************************************************************************/
        //misma forma si creamos tres metodos task por ende debemos realizar tres metodos de llamada para cada texbox
        //por lo tanto tendremos que realizar 3 llamadas para que cada textbox haga su funcion
        /********************************************************************************************/

        //metodo de llamada para el primer textbox
        private async void CallLongRunningMethod()
        {
            localhost.WebService1 obj = new localhost.WebService1(); //este metodo manda a llamar la referencia web donde almacena el procedimiento de fibonacci

            // aqui creamos una variable para mandar a llamar la etiqueta que mostrara el resultado
            string resultado = lblresultado1.Text;


            //aqui mandamos a llamar la variable pasado diciendole donde va obtener la informacion
            resultado = obj.fibonacciRecursivo(Convert.ToInt32(txt1.Text)).ToString();

            //aqui creamos el metodo de llamada para el async donde llamamos las clases anteriores para mostrar el resultado
            string result = await LongRunningMethodAsync(resultado);
        
            //simplemente mostramos el resultado del proceso anterior
            lblresultado1.Text = result;
        }

        //metodo de llamada para el segundo textbox
        private async void CallLongRunningMethod2()
        {
            localhost.WebService1 obj = new localhost.WebService1(); //este metodo manda a llamar la referencia web donde almacena el procedimiento de fibonacci

            // aqui creamos una variable para mandar a llamar la etiqueta que mostrara el resultado
            string resultado2 = lblresultado2.Text;

            //aqui mandamos a llamar la variable pasado diciendole donde va obtener la informacion
            resultado2 = obj.fibonacciRecursivo(Convert.ToInt32(txt2.Text)).ToString();

            //aqui creamos el metodo de llamada para el async donde llamamos las clases anteriores para mostrar el resultado
            string result2 = await LongRunningMethodAsync2(resultado2);

            //simplemente mostramos el resultado del proceso anterior
            lblresultado2.Text = result2;
        }

        //metodo de llamada para el tercer textbox
        private async void CallLongRunningMethod3()
        {
            try
            {

            localhost.WebService1 obj = new localhost.WebService1(); //este metodo manda a llamar la referencia web donde almacena el procedimiento de fibonacci

            // aqui creamos una variable para mandar a llamar la etiqueta que mostrara el resultado
            string resultado3 = lblresultado3.Text;

            //aqui mandamos a llamar la variable pasado diciendole donde va obtener la informacion
            resultado3 = obj.fibonacciRecursivo(Convert.ToInt32(txt3.Text)).ToString();

            //aqui creamos el metodo de llamada para el async donde llamamos las clases anteriores para mostrar el resultado
            string result3 = await LongRunningMethodAsync2(resultado3);

            //simplemente mostramos el resultado del proceso anterior
            lblresultado3.Text = result3;
            }
            catch (Exception)
            {

                MessageBox.Show("Error de datos solo se puede numeros");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //localhost.WebService1 obj = new localhost.WebService1();
            //lblresultado1.Text = obj.fibonacciRecursivo(Convert.ToInt32(txt1.Text)).ToString();

            //utilizamos la clase que tiene la operacion para mostrar resultado
            CallLongRunningMethod();
            lblresultado1.Text = "Working 1...";

        }
        /**********************************************************************************/
        // bien ya establecimos el mensaje y su funcion para ser llamado ahora necesitamos asignarle 
        //el tiempo de espera en respuesta al cliente
        /********************************************************************************************/

        //tiempo de espera para el texbox 1
        private string LongRunningMethod(string message)
        {
            // este metodo es para indicar cuantos segundos hacemos esperar al cliente para posterior mostrar el mensaje
            Thread.Sleep(1000);
            return  message;
        }

        //tiempo de espera para el texbox 2
        private string LongRunningMethod2(string message)
        {
            // este metodo es para indicar cuantos segundos hacemos esperar al cliente para posterior mostrar el mensaje
            Thread.Sleep(2000);
            return message;
        }

        //tiempo de espera para el texbox 3
        private string LongRunningMethod3(string message)
        {
            // este metodo es para indicar cuantos segundos hacemos esperar al cliente para posterior mostrar el mensaje
            Thread.Sleep(3000);
            return message;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            
            CallLongRunningMethod2();
            lblresultado2.Text = "Working 2...";

        }


        private void button3_Click(object sender, EventArgs e)
        {
            //utilizamos la clase que tiene la operacion para mostrar resultado
            CallLongRunningMethod3();
            lblresultado3.Text = "Working 3...";

        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {

        }
        private void txt1_Keypress(object sender, KeyPressEventArgs e)
        {
            // bloqueamos el ingreso de texto para evitar errores 
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("El Fibonacci solo puede hacerse con numeros", "Advertencia", MessageBoxButtons.OK);
                e.Handled = true;
                return;
            }
        }

        private void txt1_Click(object sender, EventArgs e)
        {
            
        }

        private void txt1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void txt2_KeyPress(object sender, KeyPressEventArgs e)
        {
            // bloqueamos el ingreso de texto para evitar errores 
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("El Fibonacci solo puede hacerse con numeros", "Advertencia", MessageBoxButtons.OK);
                e.Handled = true;
                return;
            }
        }

        private void txt3_KeyPress(object sender, KeyPressEventArgs e)
        {
            // bloqueamos el ingreso de texto para evitar errores 
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("El Fibonacci solo puede hacerse con numeros", "Advertencia", MessageBoxButtons.OK);
                e.Handled = true;
                return;
            }
        }
    }
    }


