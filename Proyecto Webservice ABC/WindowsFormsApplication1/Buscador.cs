﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using WindowsFormsApplication1.localhost;

namespace WindowsFormsApplication1
{
    public partial class Buscador : Form
    {
        public Buscador()
        {
            InitializeComponent();
        }
        public string buscandotealumnonombre (string snombre)
        {
            // aqui creamos el metodo a la conexion para establecer la respuesta final a un datagridview
            using (SqlConnection cnx = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                snombre = txtbuscaralumnos.Text;
                string query = "SELECT * FROM alumnos WHERE nombre LIKE @nombre + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@nombre",snombre);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                dataGridView1.DataSource = dt;
                return snombre;
            }
        }
        public string buscandotealumcontrol(string scontrol)
        {
            // aqui creamos el metodo a la conexion para establecer la respuesta final a un datagridview
            using (SqlConnection cnx = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                scontrol = txtbuscaralumnos.Text;
                string query = "SELECT * FROM alumnos WHERE no_control LIKE @no_control + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@no_control", scontrol);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                dataGridView1.DataSource = dt;
                return scontrol;
            }
        }
        public string buscandotemaestrocontrol(string scontrol)
        {
            // aqui creamos el metodo a la conexion para establecer la respuesta final a un datagridview
            using (SqlConnection cnx = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                scontrol =txtbuscaralumnos.Text;
                string query = "SELECT * FROM maestros WHERE no_control LIKE @no_control + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@no_control", scontrol);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                dataGridView1.DataSource = dt;
                return scontrol;
            }
        }
        public string buscandotemaestronombre(string snombre)
        {
            // aqui creamos el metodo a la conexion para establecer la respuesta final a un datagridview
            using (SqlConnection cnx = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                snombre = txtbuscaralumnos.Text;
                string query = "SELECT * FROM maestros WHERE nombre LIKE @nombre + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@nombre", snombre);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                dataGridView1.DataSource = dt;
                return snombre;
            }
        }
        public string buscandotemateriasmateria(string snombre)
        {
            // aqui creamos el metodo a la conexion para establecer la respuesta final a un datagridview
            using (SqlConnection cnx = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                snombre = txtbuscaralumnos.Text;
                string query = "SELECT * FROM asignaturas WHERE materia LIKE @nombre + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@nombre", snombre);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                dataGridView1.DataSource = dt;
                return snombre;
            }
        }
        public string buscandoteasignaturasnumero(string scontrol)
        {
            // aqui creamos el metodo a la conexion para establecer la respuesta final a un datagridview
            using (SqlConnection cnx = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                scontrol = txtbuscaralumnos.Text;
                string query = "SELECT * FROM asignaturas WHERE no_materia LIKE @no_control + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@no_control", scontrol);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                dataGridView1.DataSource = dt;
                return scontrol;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //aqui estamos mandando a llamar a nuestro servidor web para establecer la busqueda
            //localhost2.WebService1 obj = new localhost2.WebService1();
            //obj.ConsultaAlumnos(buscandotealumnonombre(txtbuscaralumnos.Text));
            string result;
            result = buscandotealumnonombre(txtbuscaralumnos.Text);
            return;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == "Alumnos")
            {
                lblmetodoalumno.Visible = true;
                comboBox2alumnosopcion.Visible = true;
                comboboxmaestro.Visible = false;
                comboBoxasignatura.Visible = false;
            }
            if (comboBox1.SelectedItem == "Maestros")
            {
                lblmetodoalumno.Visible = true;
                comboboxmaestro.Visible = true;
                comboBox2alumnosopcion.Visible = false;
                comboBoxasignatura.Visible = false;
            }
            if (comboBox1.SelectedItem=="Materias")
            {
                lblmetodoalumno.Visible = true;
                comboboxmaestro.Visible = false;
                comboBox2alumnosopcion.Visible = false;
                comboBoxasignatura.Visible = true;
            }
        }

        private void comboBox2alumnosopcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2alumnosopcion.SelectedItem == "Nombre")
            {
                lblalumnos1.Visible = true;
                btnalumnosnombre.Visible = true;
                txtbuscaralumnos.Visible = true;
                lblalumnosnocontrol.Visible = false;
                btnmaestronombre.Visible= false;
                btnalumnoscontrol.Visible = false;
                lblmaestronombre.Visible = false;
                lblmaestrocontrol.Visible = false;
                btnmaestrocontrol.Visible = false;
                btnmaestronombre.Visible = false;
                lblmateria.Visible = false;
                lblmateriacontrol.Visible = false;
                btnasignaturamateria.Visible = false;
                btnasignaturanumero.Visible = false;
                comboBoxasignatura.Visible = false;
                comboboxmaestro.Visible = false;
                txtbuscaralumnos.Clear();
            }
            if (comboBox2alumnosopcion.SelectedItem == "No_Control")
            {
                btnalumnosnombre.Visible = true;
                txtbuscaralumnos.Visible = true;
                btnalumnoscontrol.Visible = true;
                lblalumnosnocontrol.Visible = true;
                lblalumnos1.Visible = false;
                btnalumnosnombre.Visible = false;
                btnmaestronombre.Visible = false;
                lblmaestronombre.Visible = false;
                lblmaestrocontrol.Visible = false;
                btnmaestrocontrol.Visible = false;
                btnmaestronombre.Visible = false;
                lblmateria.Visible = false;
                lblmateriacontrol.Visible = false;
                btnasignaturamateria.Visible = false;
                btnasignaturanumero.Visible = false;
                comboBoxasignatura.Visible = false;
                comboboxmaestro.Visible = false;
                txtbuscaralumnos.Clear();
            }
        }

        private void btnalumnoscontrol_Click(object sender, EventArgs e)
        {
            //localhost2.WebService1 obj = new localhost2.WebService1();
            //obj.ConsultaAlumnos(buscandotealumcontrol(txtbuscaralumnos.Text.Trim()));
            string result;
            result = buscandotealumcontrol(txtbuscaralumnos.Text);
            return;
        }

        private void btnsemestre_Click(object sender, EventArgs e)
        {
            //localhost3.WebService1 obj = new localhost3.WebService1();
            //obj.ConsultaMaestros(buscandotealumnonombre(txtbuscaralumnos.Text.Trim()));
            string result;
            result = buscandotemaestronombre(txtbuscaralumnos.Text);
            return;
        }

        private void comboboxmaestro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboboxmaestro.SelectedItem == "Nombre")
            {
                /*etiquetas*/
                lblalumnosnocontrol.Visible = false;
                lblalumnos1.Visible = false;
                lblmaestronombre.Visible = true;
                lblmaestrocontrol.Visible = false;
                lblmateria.Visible = false;
                lblmateriacontrol.Visible = false;
                btnasignaturamateria.Visible = false;
                btnasignaturanumero.Visible = false;
                /*botones*/
                btnalumnoscontrol.Visible = false;
                btnalumnosnombre.Visible = false;
                btnmaestronombre.Visible = true;
                btnmaestrocontrol.Visible = false;
                /*textbox y combobox*/
                txtbuscaralumnos.Visible = true;
                comboBox2alumnosopcion.Visible = false;
                comboBoxasignatura.Visible = false;
                txtbuscaralumnos.Clear();
            }
            if (comboboxmaestro.SelectedItem == "No_Maestro")
            {
                /*etiquetas*/
                lblalumnosnocontrol.Visible = false;
                lblalumnos1.Visible = false;
                lblmaestronombre.Visible = false;
                lblmaestrocontrol.Visible = true;
                /*botones*/
                btnalumnoscontrol.Visible = false;
                btnalumnosnombre.Visible = false;
                btnmaestronombre.Visible = false;
                btnmaestrocontrol.Visible = true;
                lblmateria.Visible = false;
                lblmateriacontrol.Visible = false;
                btnasignaturamateria.Visible = false;
                btnasignaturanumero.Visible = false;
                /*textbox y combobox*/
                txtbuscaralumnos.Visible = true;
                comboBox2alumnosopcion.Visible = false;
                comboBoxasignatura.Visible = false;
                txtbuscaralumnos.Clear();
            }
        }

        private void btnmaestrocontrol_Click(object sender, EventArgs e)
        {
            //localhost3.WebService1 obj = new localhost3.WebService1();
            //obj.ConsultaMaestros(buscandotemaestrocontrol(txtbuscaralumnos.Text.Trim()));

            string result;
            result = buscandotemaestrocontrol(txtbuscaralumnos.Text);
            return;
        }

        private void comboBoxasignatura_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxasignatura.SelectedItem =="Materia")
            {
                /*etiquetas*/
                lblalumnosnocontrol.Visible = false;
                lblalumnos1.Visible = false;
                lblmaestronombre.Visible = false;
                lblmaestrocontrol.Visible = false;
                lblmateria.Visible = true;
                lblmateriacontrol.Visible = false;
                btnasignaturamateria.Visible = true;
                btnasignaturanumero.Visible = false;
                /*botones*/
                btnalumnoscontrol.Visible = false;
                btnalumnosnombre.Visible = false;
                btnmaestronombre.Visible = false;
                btnmaestrocontrol.Visible = false;
                /*textbox y combobox*/
                txtbuscaralumnos.Visible = true;
                comboBox2alumnosopcion.Visible = false;
                comboboxmaestro.Visible = false;
                txtbuscaralumnos.Clear();
            }
            if (comboBoxasignatura.SelectedItem=="No_Materia")
            {
                /*etiquetas*/
                lblalumnosnocontrol.Visible = false;
                lblalumnos1.Visible = false;
                lblmaestronombre.Visible = false;
                lblmaestrocontrol.Visible = false;
                lblmateria.Visible = false;
                lblmateriacontrol.Visible = true;
                btnasignaturamateria.Visible = false;
                btnasignaturanumero.Visible = true;
                /*botones*/
                btnalumnoscontrol.Visible = false;
                btnalumnosnombre.Visible = false;
                btnmaestronombre.Visible = false;
                btnmaestrocontrol.Visible = false;
                /*textbox y combobox*/
                txtbuscaralumnos.Visible = true;
                comboBox2alumnosopcion.Visible = false;
                comboboxmaestro.Visible = false;
                txtbuscaralumnos.Clear();
            }
        }

        private void btnasignaturanumero_Click(object sender, EventArgs e)
        {
            //localhost4.WebService1 obj = new localhost4.WebService1();
            //obj.ConsultaAsignaturas(buscandoteasignaturasnumero(txtbuscaralumnos.Text.Trim()));
            string result;
            result = buscandoteasignaturasnumero(txtbuscaralumnos.Text);
            return;

        }

        private void btnasignaturamateria_Click(object sender, EventArgs e)
        {
            //localhost4.WebService1 obj = new localhost4.WebService1();
            //obj.ConsultaAsignaturas(buscandotemateriasmateria(txtbuscaralumnos.Text.Trim()));
            string result;
            result = buscandotemateriasmateria(txtbuscaralumnos.Text);
            return;

        }

        private void editarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_inserccion abrir = new Alumnos_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void eliminarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_eliminar abrir = new Alumnos_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarAlumnoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Alumnos_editar abrir = new Alumnos_editar();
            abrir.Show();
            this.Hide();
        }

        private void ingresarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_inserrccion abrir = new maestro_inserrccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_editarcs abrir = new maestro_editarcs();
            abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignturas_inserccion abrir = new Asignturas_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void eliminarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar abrir = new eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarMateriaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            editar abrir = new editar();
            abrir.Show();
            this.Hide();
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void salirPorCompletoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void cerrarSesionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
         
        }
    }
    }

