﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// Microsoft.VSDesigner generó automáticamente este código fuente, versión=4.0.30319.42000.
// 
#pragma warning disable 1591

namespace WindowsFormsApplication1.localhost {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="WebService1Soap", Namespace="http://tempuri.org/")]
    public partial class WebService1 : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback insertalumnosOperationCompleted;
        
        private System.Threading.SendOrPostCallback insertMaestrosOperationCompleted;
        
        private System.Threading.SendOrPostCallback insertAsignaturasOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateAlumnoOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateMaestroOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateMateriasOperationCompleted;
        
        private System.Threading.SendOrPostCallback DeleteAlumnosOperationCompleted;
        
        private System.Threading.SendOrPostCallback DeletesMaestrosOperationCompleted;
        
        private System.Threading.SendOrPostCallback DeletesMateriasOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public WebService1() {
            this.Url = global::WindowsFormsApplication1.Properties.Settings.Default.WindowsFormsApplication1_localhost_WebService1;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event insertalumnosCompletedEventHandler insertalumnosCompleted;
        
        /// <remarks/>
        public event insertMaestrosCompletedEventHandler insertMaestrosCompleted;
        
        /// <remarks/>
        public event insertAsignaturasCompletedEventHandler insertAsignaturasCompleted;
        
        /// <remarks/>
        public event UpdateAlumnoCompletedEventHandler UpdateAlumnoCompleted;
        
        /// <remarks/>
        public event UpdateMaestroCompletedEventHandler UpdateMaestroCompleted;
        
        /// <remarks/>
        public event UpdateMateriasCompletedEventHandler UpdateMateriasCompleted;
        
        /// <remarks/>
        public event DeleteAlumnosCompletedEventHandler DeleteAlumnosCompleted;
        
        /// <remarks/>
        public event DeletesMaestrosCompletedEventHandler DeletesMaestrosCompleted;
        
        /// <remarks/>
        public event DeletesMateriasCompletedEventHandler DeletesMateriasCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/insertalumnos", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public int insertalumnos(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre) {
            object[] results = this.Invoke("insertalumnos", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        sedad,
                        ssemestre});
            return ((int)(results[0]));
        }
        
        /// <remarks/>
        public void insertalumnosAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre) {
            this.insertalumnosAsync(sno_control, snombre, sapellido_paterno, sapellido_materno, sedad, ssemestre, null);
        }
        
        /// <remarks/>
        public void insertalumnosAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre, object userState) {
            if ((this.insertalumnosOperationCompleted == null)) {
                this.insertalumnosOperationCompleted = new System.Threading.SendOrPostCallback(this.OninsertalumnosOperationCompleted);
            }
            this.InvokeAsync("insertalumnos", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        sedad,
                        ssemestre}, this.insertalumnosOperationCompleted, userState);
        }
        
        private void OninsertalumnosOperationCompleted(object arg) {
            if ((this.insertalumnosCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.insertalumnosCompleted(this, new insertalumnosCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/insertMaestros", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public int insertMaestros(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, string materias_asignadas) {
            object[] results = this.Invoke("insertMaestros", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        materias_asignadas});
            return ((int)(results[0]));
        }
        
        /// <remarks/>
        public void insertMaestrosAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, string materias_asignadas) {
            this.insertMaestrosAsync(sno_control, snombre, sapellido_paterno, sapellido_materno, materias_asignadas, null);
        }
        
        /// <remarks/>
        public void insertMaestrosAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, string materias_asignadas, object userState) {
            if ((this.insertMaestrosOperationCompleted == null)) {
                this.insertMaestrosOperationCompleted = new System.Threading.SendOrPostCallback(this.OninsertMaestrosOperationCompleted);
            }
            this.InvokeAsync("insertMaestros", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        materias_asignadas}, this.insertMaestrosOperationCompleted, userState);
        }
        
        private void OninsertMaestrosOperationCompleted(object arg) {
            if ((this.insertMaestrosCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.insertMaestrosCompleted(this, new insertMaestrosCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/insertAsignaturas", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public int insertAsignaturas(int sno_materia, string smateria, int shorario, string sMaestro_Asignado) {
            object[] results = this.Invoke("insertAsignaturas", new object[] {
                        sno_materia,
                        smateria,
                        shorario,
                        sMaestro_Asignado});
            return ((int)(results[0]));
        }
        
        /// <remarks/>
        public void insertAsignaturasAsync(int sno_materia, string smateria, int shorario, string sMaestro_Asignado) {
            this.insertAsignaturasAsync(sno_materia, smateria, shorario, sMaestro_Asignado, null);
        }
        
        /// <remarks/>
        public void insertAsignaturasAsync(int sno_materia, string smateria, int shorario, string sMaestro_Asignado, object userState) {
            if ((this.insertAsignaturasOperationCompleted == null)) {
                this.insertAsignaturasOperationCompleted = new System.Threading.SendOrPostCallback(this.OninsertAsignaturasOperationCompleted);
            }
            this.InvokeAsync("insertAsignaturas", new object[] {
                        sno_materia,
                        smateria,
                        shorario,
                        sMaestro_Asignado}, this.insertAsignaturasOperationCompleted, userState);
        }
        
        private void OninsertAsignaturasOperationCompleted(object arg) {
            if ((this.insertAsignaturasCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.insertAsignaturasCompleted(this, new insertAsignaturasCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/UpdateAlumno", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void UpdateAlumno(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre) {
            this.Invoke("UpdateAlumno", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        sedad,
                        ssemestre});
        }
        
        /// <remarks/>
        public void UpdateAlumnoAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre) {
            this.UpdateAlumnoAsync(sno_control, snombre, sapellido_paterno, sapellido_materno, sedad, ssemestre, null);
        }
        
        /// <remarks/>
        public void UpdateAlumnoAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre, object userState) {
            if ((this.UpdateAlumnoOperationCompleted == null)) {
                this.UpdateAlumnoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateAlumnoOperationCompleted);
            }
            this.InvokeAsync("UpdateAlumno", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        sedad,
                        ssemestre}, this.UpdateAlumnoOperationCompleted, userState);
        }
        
        private void OnUpdateAlumnoOperationCompleted(object arg) {
            if ((this.UpdateAlumnoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateAlumnoCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/UpdateMaestro", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void UpdateMaestro(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int materias_asignadas) {
            this.Invoke("UpdateMaestro", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        materias_asignadas});
        }
        
        /// <remarks/>
        public void UpdateMaestroAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int materias_asignadas) {
            this.UpdateMaestroAsync(sno_control, snombre, sapellido_paterno, sapellido_materno, materias_asignadas, null);
        }
        
        /// <remarks/>
        public void UpdateMaestroAsync(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int materias_asignadas, object userState) {
            if ((this.UpdateMaestroOperationCompleted == null)) {
                this.UpdateMaestroOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateMaestroOperationCompleted);
            }
            this.InvokeAsync("UpdateMaestro", new object[] {
                        sno_control,
                        snombre,
                        sapellido_paterno,
                        sapellido_materno,
                        materias_asignadas}, this.UpdateMaestroOperationCompleted, userState);
        }
        
        private void OnUpdateMaestroOperationCompleted(object arg) {
            if ((this.UpdateMaestroCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateMaestroCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/UpdateMaterias", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void UpdateMaterias(int sno_materia, string smateria, int shorario, string smaestro_asignado) {
            this.Invoke("UpdateMaterias", new object[] {
                        sno_materia,
                        smateria,
                        shorario,
                        smaestro_asignado});
        }
        
        /// <remarks/>
        public void UpdateMateriasAsync(int sno_materia, string smateria, int shorario, string smaestro_asignado) {
            this.UpdateMateriasAsync(sno_materia, smateria, shorario, smaestro_asignado, null);
        }
        
        /// <remarks/>
        public void UpdateMateriasAsync(int sno_materia, string smateria, int shorario, string smaestro_asignado, object userState) {
            if ((this.UpdateMateriasOperationCompleted == null)) {
                this.UpdateMateriasOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateMateriasOperationCompleted);
            }
            this.InvokeAsync("UpdateMaterias", new object[] {
                        sno_materia,
                        smateria,
                        shorario,
                        smaestro_asignado}, this.UpdateMateriasOperationCompleted, userState);
        }
        
        private void OnUpdateMateriasOperationCompleted(object arg) {
            if ((this.UpdateMateriasCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateMateriasCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/DeleteAlumnos", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void DeleteAlumnos(int Sno_control) {
            this.Invoke("DeleteAlumnos", new object[] {
                        Sno_control});
        }
        
        /// <remarks/>
        public void DeleteAlumnosAsync(int Sno_control) {
            this.DeleteAlumnosAsync(Sno_control, null);
        }
        
        /// <remarks/>
        public void DeleteAlumnosAsync(int Sno_control, object userState) {
            if ((this.DeleteAlumnosOperationCompleted == null)) {
                this.DeleteAlumnosOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteAlumnosOperationCompleted);
            }
            this.InvokeAsync("DeleteAlumnos", new object[] {
                        Sno_control}, this.DeleteAlumnosOperationCompleted, userState);
        }
        
        private void OnDeleteAlumnosOperationCompleted(object arg) {
            if ((this.DeleteAlumnosCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteAlumnosCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/DeletesMaestros", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void DeletesMaestros(int Sno_maestro) {
            this.Invoke("DeletesMaestros", new object[] {
                        Sno_maestro});
        }
        
        /// <remarks/>
        public void DeletesMaestrosAsync(int Sno_maestro) {
            this.DeletesMaestrosAsync(Sno_maestro, null);
        }
        
        /// <remarks/>
        public void DeletesMaestrosAsync(int Sno_maestro, object userState) {
            if ((this.DeletesMaestrosOperationCompleted == null)) {
                this.DeletesMaestrosOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletesMaestrosOperationCompleted);
            }
            this.InvokeAsync("DeletesMaestros", new object[] {
                        Sno_maestro}, this.DeletesMaestrosOperationCompleted, userState);
        }
        
        private void OnDeletesMaestrosOperationCompleted(object arg) {
            if ((this.DeletesMaestrosCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeletesMaestrosCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/DeletesMaterias", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void DeletesMaterias(string SMateria_a_eliminar) {
            this.Invoke("DeletesMaterias", new object[] {
                        SMateria_a_eliminar});
        }
        
        /// <remarks/>
        public void DeletesMateriasAsync(string SMateria_a_eliminar) {
            this.DeletesMateriasAsync(SMateria_a_eliminar, null);
        }
        
        /// <remarks/>
        public void DeletesMateriasAsync(string SMateria_a_eliminar, object userState) {
            if ((this.DeletesMateriasOperationCompleted == null)) {
                this.DeletesMateriasOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletesMateriasOperationCompleted);
            }
            this.InvokeAsync("DeletesMaterias", new object[] {
                        SMateria_a_eliminar}, this.DeletesMateriasOperationCompleted, userState);
        }
        
        private void OnDeletesMateriasOperationCompleted(object arg) {
            if ((this.DeletesMateriasCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeletesMateriasCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void insertalumnosCompletedEventHandler(object sender, insertalumnosCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class insertalumnosCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal insertalumnosCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public int Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void insertMaestrosCompletedEventHandler(object sender, insertMaestrosCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class insertMaestrosCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal insertMaestrosCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public int Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void insertAsignaturasCompletedEventHandler(object sender, insertAsignaturasCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class insertAsignaturasCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal insertAsignaturasCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public int Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void UpdateAlumnoCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void UpdateMaestroCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void UpdateMateriasCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void DeleteAlumnosCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void DeletesMaestrosCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void DeletesMateriasCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
}

#pragma warning restore 1591