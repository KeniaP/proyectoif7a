﻿namespace WindowsFormsApplication1
{
    partial class Buscador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnalumnosnombre = new System.Windows.Forms.Button();
            this.txtbuscaralumnos = new System.Windows.Forms.TextBox();
            this.lblalumnos1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2alumnosopcion = new System.Windows.Forms.ComboBox();
            this.lblmetodoalumno = new System.Windows.Forms.Label();
            this.lblalumnosnocontrol = new System.Windows.Forms.Label();
            this.btnalumnoscontrol = new System.Windows.Forms.Button();
            this.btnmaestronombre = new System.Windows.Forms.Button();
            this.comboboxmaestro = new System.Windows.Forms.ComboBox();
            this.lblmaestronombre = new System.Windows.Forms.Label();
            this.lblmaestrocontrol = new System.Windows.Forms.Label();
            this.btnmaestrocontrol = new System.Windows.Forms.Button();
            this.btnasignaturanumero = new System.Windows.Forms.Button();
            this.btnasignaturamateria = new System.Windows.Forms.Button();
            this.lblmateria = new System.Windows.Forms.Label();
            this.lblmateriacontrol = new System.Windows.Forms.Label();
            this.comboBoxasignatura = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alumnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarAlumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarAlumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarAlumnoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.maestrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresarDocenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarDocenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarMateriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarMateriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarMateriaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(34, 152);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(733, 230);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnalumnosnombre
            // 
            this.btnalumnosnombre.Location = new System.Drawing.Point(512, 123);
            this.btnalumnosnombre.Name = "btnalumnosnombre";
            this.btnalumnosnombre.Size = new System.Drawing.Size(114, 23);
            this.btnalumnosnombre.TabIndex = 1;
            this.btnalumnosnombre.Text = "Buscar Alumno";
            this.btnalumnosnombre.UseVisualStyleBackColor = true;
            this.btnalumnosnombre.Visible = false;
            this.btnalumnosnombre.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtbuscaralumnos
            // 
            this.txtbuscaralumnos.Location = new System.Drawing.Point(307, 124);
            this.txtbuscaralumnos.Name = "txtbuscaralumnos";
            this.txtbuscaralumnos.Size = new System.Drawing.Size(198, 20);
            this.txtbuscaralumnos.TabIndex = 2;
            this.txtbuscaralumnos.Visible = false;
            // 
            // lblalumnos1
            // 
            this.lblalumnos1.AutoSize = true;
            this.lblalumnos1.Location = new System.Drawing.Point(152, 129);
            this.lblalumnos1.Name = "lblalumnos1";
            this.lblalumnos1.Size = new System.Drawing.Size(139, 13);
            this.lblalumnos1.TabIndex = 3;
            this.lblalumnos1.Text = "Buscar Alumno por Nombre:";
            this.lblalumnos1.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Alumnos",
            "Maestros",
            "Materias"});
            this.comboBox1.Location = new System.Drawing.Point(219, 84);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(196, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Selecciona la opcion que desees buscar";
            // 
            // comboBox2alumnosopcion
            // 
            this.comboBox2alumnosopcion.FormattingEnabled = true;
            this.comboBox2alumnosopcion.Items.AddRange(new object[] {
            "Nombre",
            "No_Control"});
            this.comboBox2alumnosopcion.Location = new System.Drawing.Point(561, 84);
            this.comboBox2alumnosopcion.Name = "comboBox2alumnosopcion";
            this.comboBox2alumnosopcion.Size = new System.Drawing.Size(196, 21);
            this.comboBox2alumnosopcion.TabIndex = 6;
            this.comboBox2alumnosopcion.Visible = false;
            this.comboBox2alumnosopcion.SelectedIndexChanged += new System.EventHandler(this.comboBox2alumnosopcion_SelectedIndexChanged);
            // 
            // lblmetodoalumno
            // 
            this.lblmetodoalumno.AutoSize = true;
            this.lblmetodoalumno.Location = new System.Drawing.Point(441, 87);
            this.lblmetodoalumno.Name = "lblmetodoalumno";
            this.lblmetodoalumno.Size = new System.Drawing.Size(110, 13);
            this.lblmetodoalumno.TabIndex = 7;
            this.lblmetodoalumno.Text = "Selecciona el Metodo";
            this.lblmetodoalumno.Visible = false;
            // 
            // lblalumnosnocontrol
            // 
            this.lblalumnosnocontrol.AutoSize = true;
            this.lblalumnosnocontrol.Location = new System.Drawing.Point(152, 129);
            this.lblalumnosnocontrol.Name = "lblalumnosnocontrol";
            this.lblalumnosnocontrol.Size = new System.Drawing.Size(142, 13);
            this.lblalumnosnocontrol.TabIndex = 8;
            this.lblalumnosnocontrol.Text = "Buscar Alumno por # Control";
            this.lblalumnosnocontrol.Visible = false;
            // 
            // btnalumnoscontrol
            // 
            this.btnalumnoscontrol.Location = new System.Drawing.Point(512, 123);
            this.btnalumnoscontrol.Name = "btnalumnoscontrol";
            this.btnalumnoscontrol.Size = new System.Drawing.Size(114, 23);
            this.btnalumnoscontrol.TabIndex = 10;
            this.btnalumnoscontrol.Text = "Buscar # Control";
            this.btnalumnoscontrol.UseVisualStyleBackColor = true;
            this.btnalumnoscontrol.Visible = false;
            this.btnalumnoscontrol.Click += new System.EventHandler(this.btnalumnoscontrol_Click);
            // 
            // btnmaestronombre
            // 
            this.btnmaestronombre.Location = new System.Drawing.Point(512, 122);
            this.btnmaestronombre.Name = "btnmaestronombre";
            this.btnmaestronombre.Size = new System.Drawing.Size(114, 23);
            this.btnmaestronombre.TabIndex = 11;
            this.btnmaestronombre.Text = "Buscar Maestro";
            this.btnmaestronombre.UseVisualStyleBackColor = true;
            this.btnmaestronombre.Visible = false;
            this.btnmaestronombre.Click += new System.EventHandler(this.btnsemestre_Click);
            // 
            // comboboxmaestro
            // 
            this.comboboxmaestro.FormattingEnabled = true;
            this.comboboxmaestro.Items.AddRange(new object[] {
            "Nombre",
            "No_Maestro"});
            this.comboboxmaestro.Location = new System.Drawing.Point(561, 84);
            this.comboboxmaestro.Name = "comboboxmaestro";
            this.comboboxmaestro.Size = new System.Drawing.Size(196, 21);
            this.comboboxmaestro.TabIndex = 12;
            this.comboboxmaestro.Visible = false;
            this.comboboxmaestro.SelectedIndexChanged += new System.EventHandler(this.comboboxmaestro_SelectedIndexChanged);
            // 
            // lblmaestronombre
            // 
            this.lblmaestronombre.AutoSize = true;
            this.lblmaestronombre.Location = new System.Drawing.Point(152, 129);
            this.lblmaestronombre.Name = "lblmaestronombre";
            this.lblmaestronombre.Size = new System.Drawing.Size(138, 13);
            this.lblmaestronombre.TabIndex = 13;
            this.lblmaestronombre.Text = "Buscar Nombre del Maestro";
            this.lblmaestronombre.Visible = false;
            // 
            // lblmaestrocontrol
            // 
            this.lblmaestrocontrol.AutoSize = true;
            this.lblmaestrocontrol.Location = new System.Drawing.Point(181, 127);
            this.lblmaestrocontrol.Name = "lblmaestrocontrol";
            this.lblmaestrocontrol.Size = new System.Drawing.Size(109, 13);
            this.lblmaestrocontrol.TabIndex = 16;
            this.lblmaestrocontrol.Text = "Buscar # de Docente";
            this.lblmaestrocontrol.Visible = false;
            // 
            // btnmaestrocontrol
            // 
            this.btnmaestrocontrol.Location = new System.Drawing.Point(512, 121);
            this.btnmaestrocontrol.Name = "btnmaestrocontrol";
            this.btnmaestrocontrol.Size = new System.Drawing.Size(114, 23);
            this.btnmaestrocontrol.TabIndex = 17;
            this.btnmaestrocontrol.Text = "Buscar #Maestro";
            this.btnmaestrocontrol.UseVisualStyleBackColor = true;
            this.btnmaestrocontrol.Visible = false;
            this.btnmaestrocontrol.Click += new System.EventHandler(this.btnmaestrocontrol_Click);
            // 
            // btnasignaturanumero
            // 
            this.btnasignaturanumero.Location = new System.Drawing.Point(511, 122);
            this.btnasignaturanumero.Name = "btnasignaturanumero";
            this.btnasignaturanumero.Size = new System.Drawing.Size(137, 23);
            this.btnasignaturanumero.TabIndex = 18;
            this.btnasignaturanumero.Text = "Buscar # Asignatura";
            this.btnasignaturanumero.UseVisualStyleBackColor = true;
            this.btnasignaturanumero.Visible = false;
            this.btnasignaturanumero.Click += new System.EventHandler(this.btnasignaturanumero_Click);
            // 
            // btnasignaturamateria
            // 
            this.btnasignaturamateria.Location = new System.Drawing.Point(512, 121);
            this.btnasignaturamateria.Name = "btnasignaturamateria";
            this.btnasignaturamateria.Size = new System.Drawing.Size(137, 23);
            this.btnasignaturamateria.TabIndex = 19;
            this.btnasignaturamateria.Text = "Buscar Materia";
            this.btnasignaturamateria.UseVisualStyleBackColor = true;
            this.btnasignaturamateria.Visible = false;
            this.btnasignaturamateria.Click += new System.EventHandler(this.btnasignaturamateria_Click);
            // 
            // lblmateria
            // 
            this.lblmateria.AutoSize = true;
            this.lblmateria.Location = new System.Drawing.Point(209, 128);
            this.lblmateria.Name = "lblmateria";
            this.lblmateria.Size = new System.Drawing.Size(81, 13);
            this.lblmateria.TabIndex = 20;
            this.lblmateria.Text = "Buscar Materia:";
            this.lblmateria.Visible = false;
            // 
            // lblmateriacontrol
            // 
            this.lblmateriacontrol.AutoSize = true;
            this.lblmateriacontrol.Location = new System.Drawing.Point(158, 129);
            this.lblmateriacontrol.Name = "lblmateriacontrol";
            this.lblmateriacontrol.Size = new System.Drawing.Size(136, 13);
            this.lblmateriacontrol.TabIndex = 21;
            this.lblmateriacontrol.Text = "Buscar Numero de Materia:";
            this.lblmateriacontrol.Visible = false;
            // 
            // comboBoxasignatura
            // 
            this.comboBoxasignatura.FormattingEnabled = true;
            this.comboBoxasignatura.Items.AddRange(new object[] {
            "Materia",
            "No_Materia"});
            this.comboBoxasignatura.Location = new System.Drawing.Point(561, 84);
            this.comboBoxasignatura.Name = "comboBoxasignatura";
            this.comboBoxasignatura.Size = new System.Drawing.Size(196, 21);
            this.comboBoxasignatura.TabIndex = 22;
            this.comboBoxasignatura.Visible = false;
            this.comboBoxasignatura.SelectedIndexChanged += new System.EventHandler(this.comboBoxasignatura_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnosToolStripMenuItem,
            this.maestrosToolStripMenuItem,
            this.asignaturasToolStripMenuItem,
            this.cerrarSesionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(791, 24);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alumnosToolStripMenuItem
            // 
            this.alumnosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarAlumnoToolStripMenuItem,
            this.eliminarAlumnoToolStripMenuItem,
            this.editarAlumnoToolStripMenuItem1});
            this.alumnosToolStripMenuItem.Name = "alumnosToolStripMenuItem";
            this.alumnosToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.alumnosToolStripMenuItem.Text = "Alumnos";
            // 
            // editarAlumnoToolStripMenuItem
            // 
            this.editarAlumnoToolStripMenuItem.Name = "editarAlumnoToolStripMenuItem";
            this.editarAlumnoToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.editarAlumnoToolStripMenuItem.Text = "Insertar Alumno";
            this.editarAlumnoToolStripMenuItem.Click += new System.EventHandler(this.editarAlumnoToolStripMenuItem_Click);
            // 
            // eliminarAlumnoToolStripMenuItem
            // 
            this.eliminarAlumnoToolStripMenuItem.Name = "eliminarAlumnoToolStripMenuItem";
            this.eliminarAlumnoToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.eliminarAlumnoToolStripMenuItem.Text = "Eliminar Alumno";
            this.eliminarAlumnoToolStripMenuItem.Click += new System.EventHandler(this.eliminarAlumnoToolStripMenuItem_Click);
            // 
            // editarAlumnoToolStripMenuItem1
            // 
            this.editarAlumnoToolStripMenuItem1.Name = "editarAlumnoToolStripMenuItem1";
            this.editarAlumnoToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.editarAlumnoToolStripMenuItem1.Text = "Editar Alumno";
            this.editarAlumnoToolStripMenuItem1.Click += new System.EventHandler(this.editarAlumnoToolStripMenuItem1_Click);
            // 
            // maestrosToolStripMenuItem
            // 
            this.maestrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresarDocenteToolStripMenuItem,
            this.editarDatosToolStripMenuItem,
            this.eliminarDocenteToolStripMenuItem});
            this.maestrosToolStripMenuItem.Name = "maestrosToolStripMenuItem";
            this.maestrosToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.maestrosToolStripMenuItem.Text = "Maestros";
            // 
            // ingresarDocenteToolStripMenuItem
            // 
            this.ingresarDocenteToolStripMenuItem.Name = "ingresarDocenteToolStripMenuItem";
            this.ingresarDocenteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.ingresarDocenteToolStripMenuItem.Text = "Ingresar Docente";
            this.ingresarDocenteToolStripMenuItem.Click += new System.EventHandler(this.ingresarDocenteToolStripMenuItem_Click);
            // 
            // editarDatosToolStripMenuItem
            // 
            this.editarDatosToolStripMenuItem.Name = "editarDatosToolStripMenuItem";
            this.editarDatosToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.editarDatosToolStripMenuItem.Text = "Editar Datos";
            this.editarDatosToolStripMenuItem.Click += new System.EventHandler(this.editarDatosToolStripMenuItem_Click);
            // 
            // eliminarDocenteToolStripMenuItem
            // 
            this.eliminarDocenteToolStripMenuItem.Name = "eliminarDocenteToolStripMenuItem";
            this.eliminarDocenteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.eliminarDocenteToolStripMenuItem.Text = "Eliminar Docente";
            this.eliminarDocenteToolStripMenuItem.Click += new System.EventHandler(this.eliminarDocenteToolStripMenuItem_Click);
            // 
            // asignaturasToolStripMenuItem
            // 
            this.asignaturasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarMateriaToolStripMenuItem,
            this.eliminarMateriaToolStripMenuItem,
            this.editarMateriaToolStripMenuItem1});
            this.asignaturasToolStripMenuItem.Name = "asignaturasToolStripMenuItem";
            this.asignaturasToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.asignaturasToolStripMenuItem.Text = "Asignaturas";
            // 
            // editarMateriaToolStripMenuItem
            // 
            this.editarMateriaToolStripMenuItem.Name = "editarMateriaToolStripMenuItem";
            this.editarMateriaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.editarMateriaToolStripMenuItem.Text = "Insertar Materia";
            this.editarMateriaToolStripMenuItem.Click += new System.EventHandler(this.editarMateriaToolStripMenuItem_Click);
            // 
            // eliminarMateriaToolStripMenuItem
            // 
            this.eliminarMateriaToolStripMenuItem.Name = "eliminarMateriaToolStripMenuItem";
            this.eliminarMateriaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.eliminarMateriaToolStripMenuItem.Text = "Eliminar Materia";
            this.eliminarMateriaToolStripMenuItem.Click += new System.EventHandler(this.eliminarMateriaToolStripMenuItem_Click);
            // 
            // editarMateriaToolStripMenuItem1
            // 
            this.editarMateriaToolStripMenuItem1.Name = "editarMateriaToolStripMenuItem1";
            this.editarMateriaToolStripMenuItem1.Size = new System.Drawing.Size(160, 22);
            this.editarMateriaToolStripMenuItem1.Text = "Editar materia";
            this.editarMateriaToolStripMenuItem1.Click += new System.EventHandler(this.editarMateriaToolStripMenuItem1_Click);
            // 
            // cerrarSesionToolStripMenuItem
            // 
            this.cerrarSesionToolStripMenuItem.Name = "cerrarSesionToolStripMenuItem";
            this.cerrarSesionToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.cerrarSesionToolStripMenuItem.Text = "Salir";
            this.cerrarSesionToolStripMenuItem.Click += new System.EventHandler(this.cerrarSesionToolStripMenuItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(552, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Posteriormente te mostrara un textbos parte superior derecha donde debes seleccio" +
    "n porque metedo deseas buscar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(216, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(425, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Debes Seleccionar la opcion de busqueda en el texbox de arriba parte superior Izq" +
    "uierda";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(107, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "Instrucciones:";
            // 
            // Buscador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 392);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.comboBoxasignatura);
            this.Controls.Add(this.lblmateriacontrol);
            this.Controls.Add(this.lblmateria);
            this.Controls.Add(this.btnasignaturamateria);
            this.Controls.Add(this.btnasignaturanumero);
            this.Controls.Add(this.btnmaestrocontrol);
            this.Controls.Add(this.lblmaestrocontrol);
            this.Controls.Add(this.lblmaestronombre);
            this.Controls.Add(this.comboboxmaestro);
            this.Controls.Add(this.btnmaestronombre);
            this.Controls.Add(this.btnalumnoscontrol);
            this.Controls.Add(this.lblalumnosnocontrol);
            this.Controls.Add(this.lblmetodoalumno);
            this.Controls.Add(this.comboBox2alumnosopcion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lblalumnos1);
            this.Controls.Add(this.txtbuscaralumnos);
            this.Controls.Add(this.btnalumnosnombre);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Buscador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscador";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnalumnosnombre;
        private System.Windows.Forms.TextBox txtbuscaralumnos;
        private System.Windows.Forms.Label lblalumnos1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox2alumnosopcion;
        private System.Windows.Forms.Label lblmetodoalumno;
        private System.Windows.Forms.Label lblalumnosnocontrol;
        private System.Windows.Forms.Button btnalumnoscontrol;
        private System.Windows.Forms.Button btnmaestronombre;
        private System.Windows.Forms.ComboBox comboboxmaestro;
        private System.Windows.Forms.Label lblmaestronombre;
        private System.Windows.Forms.Label lblmaestrocontrol;
        private System.Windows.Forms.Button btnmaestrocontrol;
        private System.Windows.Forms.Button btnasignaturanumero;
        private System.Windows.Forms.Button btnasignaturamateria;
        private System.Windows.Forms.Label lblmateria;
        private System.Windows.Forms.Label lblmateriacontrol;
        private System.Windows.Forms.ComboBox comboBoxasignatura;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alumnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarAlumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarAlumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maestrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresarDocenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarDocenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignaturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarMateriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarMateriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarMateriaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesionToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem editarAlumnoToolStripMenuItem1;
    }
}