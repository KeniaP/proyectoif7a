﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Alumnos_eliminar : Form
    {
        public Alumnos_eliminar()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            int control = Convert.ToInt32(txtcontrol.Text);
            localhost.WebService1 obj = new localhost.WebService1(); ;
            obj.DeleteAlumnos(control);
            DialogResult dr = MessageBox.Show("El dato se ha eliminado Correctamente", "Eliminado materia", MessageBoxButtons.OK);

            if (dr == DialogResult.OK)
            {
                dataGridView1.DataSource = CargarProductosDT();
                txtcontrol.Clear();
            }
        }
        private DataTable CargarProductosDT()
        {
            try
            {

                localhost1.WebService1 obc = new localhost1.WebService1();
                DataTable dt = new DataTable();
                using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
                {
                    cnn.Open();
                    string ConsultaProductos = "SELECT id, no_control, nombre, apellido_paterno, apellido_materno,edad,semestre FROM alumnos";
                    SqlCommand cmd = new SqlCommand(ConsultaProductos, cnn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    return dt;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Alumnos_eliminar_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = CargarProductosDT();
        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editar abri = new editar();
            abri.Show();
            this.Hide();
        }

        private void eliminarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar abrir = new eliminar();
            abrir.Show();
            this.Hide();
        }

        private void insertarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignturas_inserccion abrir = new Asignturas_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void ingresarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_inserccion abrir = new Alumnos_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void ingresarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_inserrccion abrir = new maestro_inserrccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_editarcs abrir = new maestro_editarcs();
            abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_editar abrir = new Alumnos_editar();
            abrir.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Buscador abrir = new Buscador();
            abrir.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
