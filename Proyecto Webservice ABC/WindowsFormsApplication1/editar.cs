﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class editar : Form
    {
        public editar()
        {
            InitializeComponent();
        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar abrir = new eliminar();
            abrir.Show();
            this.Close();
        }

        private void insertarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignturas_inserccion abrir = new Asignturas_inserccion();
            abrir.Show();
            this.Close();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                //populate the textbox from specific value of the coordinates of column and
               txtnoasignatura.Text = row.Cells[0].Value.ToString();
               txtmateria.Text = row.Cells[1].Value.ToString();
               txthorario.Text = row.Cells[2].Value.ToString();
                txtmaestroasginado.Text = row.Cells[3].Value.ToString();
            }
        }
        private DataTable CargarProductosDT()
        {
            try
            {


                localhost1.WebService1 obc = new localhost1.WebService1();
                DataTable dt = new DataTable();
                using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
                {
                    cnn.Open();
                    string ConsultaProductos = "SELECT no_materia, materia, horario, maestro_asignado FROM asignaturas";
                    SqlCommand cmd = new SqlCommand(ConsultaProductos, cnn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                }
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void editar_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = CargarProductosDT();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                int noasignatura = Convert.ToInt32(txtnoasignatura.Text);
                int horario = Convert.ToInt32(txthorario.Text);
                localhost.WebService1 obj = new localhost.WebService1();
                obj.UpdateMaterias(noasignatura, txtmateria.Text.Trim(), horario, txtmaestroasginado.Text.Trim());
                //obj.insertAsignaturas(noasignatura, txtmateria.Text.Trim(), horario, txtmaestroasginado.Text.Trim());
                DialogResult dr = MessageBox.Show("Se Ha Actualizado El registro", "Actualizando Materia", MessageBoxButtons.OK);

                if (dr == DialogResult.OK)
                {
                    dataGridView1.DataSource = CargarProductosDT();
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error al ingresar, revisa tus datos correctamente");
            }
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void ingresarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_inserrccion abrir = new maestro_inserrccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_editarcs abrir = new maestro_editarcs();
            abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Buscador abrir = new Buscador();
            abrir.Show();
            this.Hide();
        }
    }
}
