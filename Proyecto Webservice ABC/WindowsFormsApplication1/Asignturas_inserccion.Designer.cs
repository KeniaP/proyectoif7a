﻿namespace WindowsFormsApplication1
{
    partial class Asignturas_inserccion
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asignturas_inserccion));
            this.txthorario = new System.Windows.Forms.TextBox();
            this.txtmateria = new System.Windows.Forms.TextBox();
            this.txtnoasignatura = new System.Windows.Forms.TextBox();
            this.txtmaestroasginado = new System.Windows.Forms.TextBox();
            this.btninsertar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbl1insertar = new System.Windows.Forms.Label();
            this.lbl2insertar = new System.Windows.Forms.Label();
            this.lbl3insertar = new System.Windows.Forms.Label();
            this.lbl4insertar = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alumnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresarAlumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarAlumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarAlumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maestrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresarDocenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarDocenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarMateriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarMateriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnmostrardatos = new System.Windows.Forms.Button();
            this.datamostrandolosdatos = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnclear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datamostrandolosdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txthorario
            // 
            this.txthorario.Location = new System.Drawing.Point(298, 87);
            this.txthorario.Name = "txthorario";
            this.txthorario.Size = new System.Drawing.Size(100, 20);
            this.txthorario.TabIndex = 3;
            this.txthorario.WordWrap = false;
            // 
            // txtmateria
            // 
            this.txtmateria.Location = new System.Drawing.Point(167, 87);
            this.txtmateria.Name = "txtmateria";
            this.txtmateria.Size = new System.Drawing.Size(100, 20);
            this.txtmateria.TabIndex = 2;
            this.txtmateria.WordWrap = false;
            // 
            // txtnoasignatura
            // 
            this.txtnoasignatura.Location = new System.Drawing.Point(43, 87);
            this.txtnoasignatura.Name = "txtnoasignatura";
            this.txtnoasignatura.Size = new System.Drawing.Size(100, 20);
            this.txtnoasignatura.TabIndex = 1;
            this.txtnoasignatura.WordWrap = false;
            // 
            // txtmaestroasginado
            // 
            this.txtmaestroasginado.Location = new System.Drawing.Point(429, 87);
            this.txtmaestroasginado.Name = "txtmaestroasginado";
            this.txtmaestroasginado.Size = new System.Drawing.Size(100, 20);
            this.txtmaestroasginado.TabIndex = 4;
            this.txtmaestroasginado.WordWrap = false;
            // 
            // btninsertar
            // 
            this.btninsertar.Location = new System.Drawing.Point(127, 122);
            this.btninsertar.Name = "btninsertar";
            this.btninsertar.Size = new System.Drawing.Size(183, 23);
            this.btninsertar.TabIndex = 5;
            this.btninsertar.Text = "Insertar Materia";
            this.btninsertar.UseVisualStyleBackColor = true;
            this.btninsertar.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(17, 151);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(526, 148);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // lbl1insertar
            // 
            this.lbl1insertar.AutoSize = true;
            this.lbl1insertar.Location = new System.Drawing.Point(40, 71);
            this.lbl1insertar.Name = "lbl1insertar";
            this.lbl1insertar.Size = new System.Drawing.Size(105, 13);
            this.lbl1insertar.TabIndex = 6;
            this.lbl1insertar.Text = "Ingresa # de Materia";
            // 
            // lbl2insertar
            // 
            this.lbl2insertar.AutoSize = true;
            this.lbl2insertar.Location = new System.Drawing.Point(168, 71);
            this.lbl2insertar.Name = "lbl2insertar";
            this.lbl2insertar.Size = new System.Drawing.Size(96, 13);
            this.lbl2insertar.TabIndex = 7;
            this.lbl2insertar.Text = "Nombre de materia";
            // 
            // lbl3insertar
            // 
            this.lbl3insertar.AutoSize = true;
            this.lbl3insertar.Location = new System.Drawing.Point(294, 71);
            this.lbl3insertar.Name = "lbl3insertar";
            this.lbl3insertar.Size = new System.Drawing.Size(104, 13);
            this.lbl3insertar.TabIndex = 8;
            this.lbl3insertar.Text = "Horario de la materia";
            // 
            // lbl4insertar
            // 
            this.lbl4insertar.AutoSize = true;
            this.lbl4insertar.Location = new System.Drawing.Point(433, 71);
            this.lbl4insertar.Name = "lbl4insertar";
            this.lbl4insertar.Size = new System.Drawing.Size(92, 13);
            this.lbl4insertar.TabIndex = 9;
            this.lbl4insertar.Text = "Maestro Asignado";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnosToolStripMenuItem,
            this.maestrosToolStripMenuItem,
            this.asignaturasToolStripMenuItem,
            this.cerrarSesionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(563, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alumnosToolStripMenuItem
            // 
            this.alumnosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresarAlumnoToolStripMenuItem,
            this.editarAlumnoToolStripMenuItem,
            this.eliminarAlumnoToolStripMenuItem});
            this.alumnosToolStripMenuItem.Name = "alumnosToolStripMenuItem";
            this.alumnosToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.alumnosToolStripMenuItem.Text = "Alumnos";
            // 
            // ingresarAlumnoToolStripMenuItem
            // 
            this.ingresarAlumnoToolStripMenuItem.Name = "ingresarAlumnoToolStripMenuItem";
            this.ingresarAlumnoToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.ingresarAlumnoToolStripMenuItem.Text = "Ingresar Alumno";
            this.ingresarAlumnoToolStripMenuItem.Click += new System.EventHandler(this.ingresarAlumnoToolStripMenuItem_Click);
            // 
            // editarAlumnoToolStripMenuItem
            // 
            this.editarAlumnoToolStripMenuItem.Name = "editarAlumnoToolStripMenuItem";
            this.editarAlumnoToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.editarAlumnoToolStripMenuItem.Text = "Editar Alumno";
            this.editarAlumnoToolStripMenuItem.Click += new System.EventHandler(this.editarAlumnoToolStripMenuItem_Click);
            // 
            // eliminarAlumnoToolStripMenuItem
            // 
            this.eliminarAlumnoToolStripMenuItem.Name = "eliminarAlumnoToolStripMenuItem";
            this.eliminarAlumnoToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.eliminarAlumnoToolStripMenuItem.Text = "Eliminar Alumno";
            this.eliminarAlumnoToolStripMenuItem.Click += new System.EventHandler(this.eliminarAlumnoToolStripMenuItem_Click);
            // 
            // maestrosToolStripMenuItem
            // 
            this.maestrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresarDocenteToolStripMenuItem,
            this.editarDatosToolStripMenuItem,
            this.eliminarDocenteToolStripMenuItem});
            this.maestrosToolStripMenuItem.Name = "maestrosToolStripMenuItem";
            this.maestrosToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.maestrosToolStripMenuItem.Text = "Maestros";
            // 
            // ingresarDocenteToolStripMenuItem
            // 
            this.ingresarDocenteToolStripMenuItem.Name = "ingresarDocenteToolStripMenuItem";
            this.ingresarDocenteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.ingresarDocenteToolStripMenuItem.Text = "Ingresar Docente";
            this.ingresarDocenteToolStripMenuItem.Click += new System.EventHandler(this.ingresarDocenteToolStripMenuItem_Click);
            // 
            // editarDatosToolStripMenuItem
            // 
            this.editarDatosToolStripMenuItem.Name = "editarDatosToolStripMenuItem";
            this.editarDatosToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.editarDatosToolStripMenuItem.Text = "Editar Datos";
            this.editarDatosToolStripMenuItem.Click += new System.EventHandler(this.editarDatosToolStripMenuItem_Click);
            // 
            // eliminarDocenteToolStripMenuItem
            // 
            this.eliminarDocenteToolStripMenuItem.Name = "eliminarDocenteToolStripMenuItem";
            this.eliminarDocenteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.eliminarDocenteToolStripMenuItem.Text = "Eliminar Docente";
            this.eliminarDocenteToolStripMenuItem.Click += new System.EventHandler(this.eliminarDocenteToolStripMenuItem_Click);
            // 
            // asignaturasToolStripMenuItem
            // 
            this.asignaturasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarMateriaToolStripMenuItem,
            this.eliminarMateriaToolStripMenuItem});
            this.asignaturasToolStripMenuItem.Name = "asignaturasToolStripMenuItem";
            this.asignaturasToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.asignaturasToolStripMenuItem.Text = "Asignaturas";
            this.asignaturasToolStripMenuItem.Click += new System.EventHandler(this.asignaturasToolStripMenuItem_Click);
            // 
            // editarMateriaToolStripMenuItem
            // 
            this.editarMateriaToolStripMenuItem.Name = "editarMateriaToolStripMenuItem";
            this.editarMateriaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.editarMateriaToolStripMenuItem.Text = "Editar Materia";
            this.editarMateriaToolStripMenuItem.Click += new System.EventHandler(this.editarMateriaToolStripMenuItem_Click);
            // 
            // eliminarMateriaToolStripMenuItem
            // 
            this.eliminarMateriaToolStripMenuItem.Name = "eliminarMateriaToolStripMenuItem";
            this.eliminarMateriaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.eliminarMateriaToolStripMenuItem.Text = "Eliminar Materia";
            this.eliminarMateriaToolStripMenuItem.Click += new System.EventHandler(this.eliminarMateriaToolStripMenuItem_Click);
            // 
            // cerrarSesionToolStripMenuItem
            // 
            this.cerrarSesionToolStripMenuItem.Name = "cerrarSesionToolStripMenuItem";
            this.cerrarSesionToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.cerrarSesionToolStripMenuItem.Text = "Salir";
            this.cerrarSesionToolStripMenuItem.Click += new System.EventHandler(this.cerrarSesionToolStripMenuItem_Click);
            // 
            // btnmostrardatos
            // 
            this.btnmostrardatos.Location = new System.Drawing.Point(127, 122);
            this.btnmostrardatos.Name = "btnmostrardatos";
            this.btnmostrardatos.Size = new System.Drawing.Size(183, 23);
            this.btnmostrardatos.TabIndex = 7;
            this.btnmostrardatos.Text = "Mostrar Todas Las Asignaturas";
            this.btnmostrardatos.UseVisualStyleBackColor = true;
            this.btnmostrardatos.Visible = false;
            this.btnmostrardatos.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // datamostrandolosdatos
            // 
            this.datamostrandolosdatos.BackgroundColor = System.Drawing.SystemColors.Control;
            this.datamostrandolosdatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datamostrandolosdatos.Location = new System.Drawing.Point(17, 151);
            this.datamostrandolosdatos.Name = "datamostrandolosdatos";
            this.datamostrandolosdatos.Size = new System.Drawing.Size(526, 148);
            this.datamostrandolosdatos.TabIndex = 13;
            this.datamostrandolosdatos.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(463, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 49;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(356, 122);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(143, 23);
            this.btnclear.TabIndex = 6;
            this.btnclear.Text = "Ingresar Otro";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Visible = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // Asignturas_inserccion
            // 
            this.ClientSize = new System.Drawing.Size(563, 311);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.datamostrandolosdatos);
            this.Controls.Add(this.btnmostrardatos);
            this.Controls.Add(this.lbl4insertar);
            this.Controls.Add(this.lbl3insertar);
            this.Controls.Add(this.lbl2insertar);
            this.Controls.Add(this.lbl1insertar);
            this.Controls.Add(this.btninsertar);
            this.Controls.Add(this.txtmaestroasginado);
            this.Controls.Add(this.txtnoasignatura);
            this.Controls.Add(this.txtmateria);
            this.Controls.Add(this.txthorario);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dataGridView1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Asignturas_inserccion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Asignturas_inserccion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datamostrandolosdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txthorario;
        private System.Windows.Forms.TextBox txtmateria;
        private System.Windows.Forms.TextBox txtnoasignatura;
        private System.Windows.Forms.TextBox txtmaestroasginado;
        private System.Windows.Forms.Button btninsertar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbl1insertar;
        private System.Windows.Forms.Label lbl2insertar;
        private System.Windows.Forms.Label lbl3insertar;
        private System.Windows.Forms.Label lbl4insertar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alumnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresarAlumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarAlumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarAlumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maestrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresarDocenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarDocenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignaturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarMateriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarMateriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesionToolStripMenuItem;
        private System.Windows.Forms.Button btnmostrardatos;
        private System.Windows.Forms.DataGridView datamostrandolosdatos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnclear;
    }
}

