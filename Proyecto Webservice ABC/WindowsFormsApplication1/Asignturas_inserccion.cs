﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Asignturas_inserccion : Form
    {
        public Asignturas_inserccion()
        {
            InitializeComponent();
        }
        private void Asignturas_inserccion_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            
            int noasignatura = Convert.ToInt32(txtnoasignatura.Text);
            int horario = Convert.ToInt32(txthorario.Text);
            localhost.WebService1 obj = new localhost.WebService1();
            obj.insertAsignaturas(noasignatura, txtmateria.Text.Trim(), horario, txtmaestroasginado.Text.Trim());
            //dataGridView1.Rows.Add(txtnoasignatura.Text,txtmateria.Text,txthorario.Text,txtmaestroasginado.Text);

            DataTable dt = new DataTable();
            DataRow Row1;
            //DataRow Row2;
            dt.Columns.Add(new DataColumn("no_asignatura", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("materia", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("horario", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("maestro_asignado", System.Type.GetType("System.String")));

            Row1 = dt.NewRow();
            Row1["no_asignatura"] = this.txtnoasignatura.Text;
            Row1["materia"] = this.txtmateria.Text;
            Row1["horario"] = this.txthorario.Text;
            Row1["maestro_asignado"] = this.txtmaestroasginado.Text;
            dt.Rows.Add(Row1);

            dataGridView1.DataSource = dt;
                btnmostrardatos.Visible = true;
                btninsertar.Visible = false;
                btnclear.Visible = true;
                MessageBox.Show("Elemento guardado");

            }
            catch (Exception)
            {
                MessageBox.Show("Error al ingresar, revisa tus datos correctamente");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void insertarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private DataTable CargarProductosDT()
        {
            localhost1.WebService1 obc = new localhost1.WebService1();
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                cnn.Open();
                string ConsultaProductos = "SELECT id_asignatura, no_materia, materia, horario, maestro_asignado FROM asignaturas";
                SqlCommand cmd = new SqlCommand(ConsultaProductos, cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            datamostrandolosdatos.Visible = true;
            datamostrandolosdatos.DataSource = CargarProductosDT();
         
        }

        private void eliminarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar delete = new eliminar();
            delete.Show();
            this.Hide();
        }

        private void asignaturasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editar abrir = new editar();
            abrir.Show();
            this.Hide();
        }

        private void ingresarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_inserccion abrir = new Alumnos_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void editarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_editar abrir = new Alumnos_editar();
            abrir.Show();
            this.Hide();
        }

        private void eliminarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_eliminar abrir = new Alumnos_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void ingresarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_inserrccion abrir = new maestro_inserrccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_editarcs abrir = new maestro_editarcs();
            abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Buscador abrir = new Buscador();
            abrir.Show();
            this.Hide();
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            txtnoasignatura.Clear();
            txtmateria.Clear();
            txtmaestroasginado.Clear();
            txthorario.Clear();
            btnmostrardatos.Visible = false;
            btninsertar.Visible = true;
            
        }
    }
}
