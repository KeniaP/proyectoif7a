﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class maestro_editarcs : Form
    {
        public maestro_editarcs()
        {
            InitializeComponent();
        }

        private void maestro_editarcs_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = CargarProductosDT();
        }
        private DataTable CargarProductosDT()
        {
            localhost1.WebService1 obc = new localhost1.WebService1();
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                cnn.Open();
                string ConsultaProductos = "SELECT id, no_control, nombre, apellido_paterno, apellido_materno,materias_asignadas FROM maestros";
                SqlCommand cmd = new SqlCommand(ConsultaProductos, cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                int no_control = Convert.ToInt32(txtcontrol.Text);
                int materias = Convert.ToInt32(txtmaterias.Text);
                localhost.WebService1 obj = new localhost.WebService1();
                obj.UpdateMaestro(no_control, txtnombre.Text.Trim(), txtpaterno.Text.Trim(), txtmaterno.Text.Trim(), materias);
                //obj.insertAsignaturas(noasignatura, txtmateria.Text.Trim(), horario, txtmaestroasginado.Text.Trim());
                DialogResult dr = MessageBox.Show("Se Ha Actualizado El registro", "Actualizando Docente", MessageBoxButtons.OK);

                if (dr == DialogResult.OK)
                {
                    dataGridView1.DataSource = CargarProductosDT();
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error al ingresar, revisa tus datos correctamente");
            }

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                //populate the textbox from specific value of the coordinates of column and
               txtcontrol.Text = row.Cells[1].Value.ToString();
               txtnombre.Text = row.Cells[2].Value.ToString();
               txtpaterno.Text = row.Cells[3].Value.ToString();
               txtmaterno.Text = row.Cells[4].Value.ToString();
                txtmaterias.Text = row.Cells[5].Value.ToString();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                //populate the textbox from specific value of the coordinates of column and
                txtcontrol.Text = row.Cells[1].Value.ToString();
                txtnombre.Text = row.Cells[2].Value.ToString();
                txtpaterno.Text = row.Cells[3].Value.ToString();
                txtmaterno.Text = row.Cells[4].Value.ToString();
                txtmaterias.Text = row.Cells[4].Value.ToString();
            }
        }

        private void editarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_editar abrir = new Alumnos_editar();
            abrir.Show();
            this.Hide();
        }

        private void eliminarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_eliminar abrir = new Alumnos_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void insertarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_inserccion abrir = new Alumnos_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_inserrccion abrir = new maestro_inserrccion();
                abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignturas_inserccion abrir = new Asignturas_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void eliminarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar abrir = new eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarMateriaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            editar abrir = new editar();
            abrir.Show();
            this.Hide();
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Buscador abrir = new Buscador();
            abrir.Show();
            this.Hide();
        }
    }
}
