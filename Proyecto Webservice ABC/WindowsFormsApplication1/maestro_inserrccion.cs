﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class maestro_inserrccion : Form
    {
        public maestro_inserrccion()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                int no_control = Convert.ToInt32(txtcontrol.Text);
                int edad = Convert.ToInt32(txtmaterias.Text);
                localhost.WebService1 obj = new localhost.WebService1();
                obj.insertMaestros(no_control, txtnombre.Text.Trim(), txtpaterno.Text.Trim(), txtmaterno.Text.Trim(), txtmaterias.Text.Trim());
                //dataGridView1.Rows.Add(txtnoasignatura.Text,txtmateria.Text,txthorario.Text,txtmaestroasginado.Text);

                DataTable dt = new DataTable();
                DataRow Row1;
                //DataRow Row2;
                dt.Columns.Add(new DataColumn("no_control", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("nombre", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("apellido_paterno", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("apellido_materno", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("materias_asignadas", System.Type.GetType("System.String")));

                Row1 = dt.NewRow();
                Row1["no_control"] = this.txtcontrol.Text;
                Row1["nombre"] = this.txtnombre.Text;
                Row1["apellido_paterno"] = this.txtpaterno.Text;
                Row1["apellido_materno"] = this.txtmaterno.Text;
                Row1["materias_asignadas"] = this.txtmaterias.Text;
                dt.Rows.Add(Row1);

                dataGridView1.DataSource = dt;
                btnmostraralumnos.Visible = true;
                btnclear.Visible = true;

                MessageBox.Show("Elemento guardado");

            }
            catch (Exception)
            {
                MessageBox.Show("Error al ingresar, revisa tus datos correctamente");
            }
        }
        private DataTable CargarProductosDT()
        {
            localhost1.WebService1 obc = new localhost1.WebService1();
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=Escuela_Kenia;Integrated Security=True"))
            {
                cnn.Open();
                string ConsultaProductos = "SELECT id, no_control, nombre, apellido_paterno, apellido_materno,materias_asignadas FROM maestros";
                SqlCommand cmd = new SqlCommand(ConsultaProductos, cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }
        private void btnmostraralumnos_Click(object sender, EventArgs e)
        {
            dataGridView2.Visible = true;
            dataGridView2.DataSource = CargarProductosDT();
        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignturas_inserccion abrir = new Asignturas_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void eliminarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar abrir = new eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarMateriaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            editar abrir = new editar();
            abrir.Show();
            this.Hide();
        }

        private void editarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_editar abrir = new Alumnos_editar();
            abrir.Show();
            this.Hide();
        }

        private void eliminarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_eliminar abrir = new Alumnos_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void insertarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_inserccion abrir = new Alumnos_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_editarcs abrir = new maestro_editarcs();
            abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Buscador abrir = new Buscador();
            abrir.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtcontrol.Clear();
            txtnombre.Clear();
            txtpaterno.Clear();
            txtmaterno.Clear();
            txtmaterias.Clear();
            btnmostraralumnos.Visible = false;
            btningreso.Visible = true;
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }
    }
    }

