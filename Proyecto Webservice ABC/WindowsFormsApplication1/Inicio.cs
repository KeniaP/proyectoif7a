﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void insertarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void insertarMateriaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Asignturas_inserccion abrir = new Asignturas_inserccion();
            abrir.Show();
            this.Hide();
        }

        private void eliminarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminar abrir = new eliminar();
            abrir.Show();
            this.Hide();
        }

        private void editarMateriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editar abrir = new editar();
            abrir.Show();
            this.Hide();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void insertarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_inserccion abrir = new Alumnos_inserccion();
            abrir.Show();
            this.Hide();
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            string usuario = "admin";
            string contra = "123";
            if (txtuser.Text == usuario && txtpass.Text == contra)
            {
                menuStrip1.Visible = true;
                txtpass.Visible = false;
                txtuser.Visible = false;
                btniniciarsesion.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                MessageBox.Show("Bienvenido usuario admin");

            }
        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }

        private void editarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_editar abrir = new Alumnos_editar();
            abrir.Show();
            this.Hide();
        }

        private void eliminarAlumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumnos_eliminar abrir = new Alumnos_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void insertarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_inserrccion abrir = new maestro_inserrccion();
            abrir.Show();
            this.Hide();
        }

        private void editarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_editarcs abrir = new maestro_editarcs();
            abrir.Show();
            this.Hide();
        }

        private void eliminarDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            maestro_eliminar abrir = new maestro_eliminar();
            abrir.Show();
            this.Hide();
        }

        private void txtpass_Click(object sender, EventArgs e)
        {
            txtpass.UseSystemPasswordChar = true;
        }
    }
}
