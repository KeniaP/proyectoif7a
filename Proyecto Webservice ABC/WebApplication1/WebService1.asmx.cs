﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.ComponentModel;
using System.Drawing;
using System.Text;


namespace WebApplication1
{
    /// <summary>
    /// Fibonacci Sequence generator via ASMX Web Service
    /// Tudor Laptes, 05/18/17
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        public WebService1()
        {

            //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
            //InitializeComponent(); 
        }

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hola a todos";
        //}
        /****************************************************************************************************************/
        //                                                      INSERCCION DE DATOS
        //
        /****************************************************************************************************************/

        [System.Web.Services.WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod()]
        public int insertalumnos(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre)
        {
            string connStr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            var cmd = new SqlCommand("Insert into alumnos(no_control,nombre, apellido_paterno, apellido_materno,edad,semestre) values('" + sno_control + "','" + snombre + "','" + sapellido_paterno + "','" + sapellido_materno + "','" + sedad + "','" + ssemestre + "')", con);
            int row = cmd.ExecuteNonQuery();
            return row;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [WebMethod]

        public int insertMaestros(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, string materias_asignadas)
        {
            string connStr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            var cmd = new SqlCommand("Insert into maestros(no_control,nombre, apellido_paterno, apellido_materno,materias_asignadas) values('" + sno_control + "','" + snombre + "','" + sapellido_paterno + "','" + sapellido_materno + "','" + materias_asignadas + "')", con);
            int row = cmd.ExecuteNonQuery();
            return row;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [WebMethod]

        public int insertAsignaturas(int sno_materia, string smateria, int shorario, string sMaestro_Asignado)
        {
            string connStr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            var cmd = new SqlCommand("Insert into asignaturas(no_materia,materia,horario, maestro_asignado) values('" + sno_materia + "','" + smateria + "','" + shorario + "','" + sMaestro_Asignado + "')", con);
            int row = cmd.ExecuteNonQuery();
            return row;
        }
        /****************************************************************************************************************/
        //                                                       ACTUALIZAR DATOS
        //
        /****************************************************************************************************************/
        [WebMethod]
        public void UpdateAlumno(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int sedad, int ssemestre)

        {
            string connStr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE alumnos SET nombre = @nombre, apellido_paterno = @apellido_paterno, apellido_materno = @apellido_materno, edad = @edad, semestre = @semestre WHERE no_control = @no_control"))
                {
                    cmd.Parameters.AddWithValue("@no_control", sno_control);
                    cmd.Parameters.AddWithValue("@nombre", snombre);
                    cmd.Parameters.AddWithValue("@apellido_paterno", sapellido_paterno);
                    cmd.Parameters.AddWithValue("@apellido_materno", sapellido_materno);
                    cmd.Parameters.AddWithValue("@edad", sedad);
                    cmd.Parameters.AddWithValue("@semestre", ssemestre);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [WebMethod]
        public void UpdateMaestro(int sno_control, string snombre, string sapellido_paterno, string sapellido_materno, int materias_asignadas)

        {
            string connStr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE maestros SET nombre = @nombre, apellido_paterno = @apellido_paterno, apellido_materno = @apellido_materno, materias_asignadas = @materias_asignadas WHERE no_control = @no_control"))
                {
                    cmd.Parameters.AddWithValue("@no_control", sno_control);
                    cmd.Parameters.AddWithValue("@nombre", snombre);
                    cmd.Parameters.AddWithValue("@apellido_paterno", sapellido_paterno);
                    cmd.Parameters.AddWithValue("@apellido_materno", sapellido_materno);
                    cmd.Parameters.AddWithValue("@materias_asignadas", materias_asignadas);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();

                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [WebMethod]
        public void UpdateMaterias(int sno_materia, string smateria, int shorario, string smaestro_asignado)

        {
            string connStr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE asignaturas SET materia = @materia, horario = @horario, maestro_asignado = @maestro_asignado WHERE no_materia = @no_materia"))
                {
                    cmd.Parameters.AddWithValue("@no_materia", sno_materia);
                    cmd.Parameters.AddWithValue("@materia", smateria);
                    cmd.Parameters.AddWithValue("@horario", shorario);
                    cmd.Parameters.AddWithValue("@maestro_asignado", smaestro_asignado);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();

                }
            }
        }
        /****************************************************************************************************************/
        //                                                 Eliminar datos
        //
        /****************************************************************************************************************/
        [WebMethod]
        public void DeleteAlumnos(int Sno_control)
        {
            string constr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM alumnos WHERE no_control = @no_control"))
                {
                    cmd.Parameters.AddWithValue("@no_control", Sno_control);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        [WebMethod]
        public void DeletesMaestros(int Sno_maestro)
        {
            string constr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM maestros WHERE no_control = @no_control"))
                {
                    cmd.Parameters.AddWithValue("@no_control", Sno_maestro);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        [WebMethod]
        public void DeletesMaterias(string SMateria_a_eliminar)
        {
            string constr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM asignaturas WHERE materia = @materia"))
                {
                    cmd.Parameters.AddWithValue("@materia", SMateria_a_eliminar);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        /****************************************************************************************************************/
        //                                                BUSQUEDA DE DATOS
        //
        /****************************************************************************************************************/

        [WebMethod]
        public string ConsultaAlumnos(string sNombre)
        {
            string constr = ConfigurationManager.ConnectionStrings["MyDatabaseConnectionString1"].ConnectionString;
            using (SqlConnection cnx = new SqlConnection(constr))
            {

                string query = "SELECT * FROM alumnos WHERE nombre LIKE @nombre + '%'";
                SqlCommand cmd = new SqlCommand(query, cnx);
                cmd.Parameters.AddWithValue("@nombre", sNombre);
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return sNombre;
                //SqlConnection conexion = new SqlConnection("server=(local) ; database=Escuela_Kenia; integrated security = true");
                //SqlDataAdapter da = new SqlDataAdapter("Select * from alumnos", conexion);
                //DataSet ds = new DataSet();
                //da.Fill(ds);
                //return ds;
            }
        }
    }
}


    
