create database Escuela_Kenia
Use Escuela_Kenia

create table alumnos
(
id int identity not null,
no_control int not null,
nombre varchar(30) not null,
apellido_paterno varchar(30) not null,
apellido_materno varchar(30) not null,
edad int,
semestre int not null
)

insert into alumnos(no_control, nombre, apellido_paterno, apellido_materno, edad, semestre) values(14212338, 'kenia', 'perez', 'lopez',22,7)
insert into alumnos(no_control, nombre, apellido_paterno, apellido_materno, edad, semestre) values(14212341, 'fatima', 'ruiz', 'garcia',23,6)
insert into alumnos(no_control, nombre, apellido_paterno, apellido_materno, edad, semestre) values(14212332, 'juan', 'lopez', 'madrid',22,6)

select * from alumnos
drop table alumnos


create table maestros
(
id int identity not null,
no_control int not null,
nombre varchar(30) not null,
apellido_paterno varchar(30) not null,
apellido_materno varchar(30) not null,
materias_asignadas int not null
)

insert into maestros(no_control, nombre, apellido_paterno, apellido_materno, materias_asignadas) values (120, 'luis','vazquez','ups',1)
insert into maestros(no_control, nombre, apellido_paterno, apellido_materno, materias_asignadas) values (122, 'rosana','gutierrez','montoya',5)
insert into maestros(no_control, nombre, apellido_paterno, apellido_materno, materias_asignadas) values (121, 'marco','rodigruez','rodriguez',2)

select * from maestros


create table asignaturas
(
id_asignatura int identity not null,
no_materia int not null,
materia varchar(30) not null,
horario int,
maestro_asignado varchar (30)
)

select * from asignaturas

drop table asignaturas
insert into asignaturas(no_materia, materia, horario, maestro_asignado) values(11,'programacion en ambiente',6,'luis')
insert into asignaturas(no_materia, materia, horario, maestro_asignado) values(12,'calidad',6,'rosana')
insert into asignaturas(no_materia, materia, horario, maestro_asignado) values(13,'programacio ',6,'marco')

create table usuarios
(
id_user int identity not null,
 usuario varchar(30) not null,
 contraseņa varchar(30) not null
)
select * from usuarios
insert into usuarios(usuario,contraseņa) values('admin','123');

