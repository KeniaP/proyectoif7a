﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ZOOKENIA
{
    /// <summary>
    /// Descripción breve de WebKenia
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebKenia : System.Web.Services.WebService
    {

        /*[WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }*/

        [WebMethod]
        public string Felino(string Color_de_ojos, string Numero_de_patas, string Tipo_de_felino, string Peso, string Estatura, string Color_de_piel)
        {
            return ("Tipo de felino:\n\r" + " " + Tipo_de_felino + ", " + "color de ojos: " + " " + Color_de_ojos + ", " + "Numero de patas:" + " " + Numero_de_patas + ", " + "Peso: " + " " + Peso + ", " + "Estatura:" + Estatura + ", " + "Color de piel" + " "  + Color_de_piel + ".");
        }
        [WebMethod]
        public string Reptil(string Color_de_ojos, string Numero_de_patas, string Tipo_de_reptil, string Peso, string Estatura, string Color_de_piel)
        {
            return ("Tipo de reptil:\n\r" + " " + Tipo_de_reptil + ", " + " Color de ojos:" + Color_de_ojos + "," + " Numero de patas:" + Numero_de_patas + "," + "Peso:" + Peso + ", " + "Estatura:" + Estatura + ", " + "Color de piel:" + Color_de_piel + ".");
        }
        [WebMethod]
        public string Ave(string color_de_ojos, string numero_de_patas, string tipo_ave, string Peso, string Estatura, string Color_de_plumas)
        {
            return ("Tipo de ave:\n\r" + tipo_ave + ", " + "Color de ojos:\n\r " + " " + color_de_ojos + " , " + "Numero de patas:\n\r" + numero_de_patas + " , " + "Peso:\n\r " + Peso + " , " + "Estatura:\n\r " + Estatura + " , " + "Color de plumas:\n\r " + Color_de_plumas + ".");
        }
    }
}

